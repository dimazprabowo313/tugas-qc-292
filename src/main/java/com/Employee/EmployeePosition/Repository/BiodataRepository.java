package com.Employee.EmployeePosition.Repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.Employee.EmployeePosition.Model.Biodata;

public interface BiodataRepository extends JpaRepository<Biodata, Long>{

}
