package com.Employee.EmployeePosition.Repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.Employee.EmployeePosition.Model.Position;

public interface PositionRepository extends JpaRepository<Position, Long>{

}
