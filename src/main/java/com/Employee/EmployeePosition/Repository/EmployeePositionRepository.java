package com.Employee.EmployeePosition.Repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.Employee.EmployeePosition.Model.EmployeePosition;

public interface EmployeePositionRepository extends JpaRepository<EmployeePosition, Long> {

	@Query("From EmployeePosition WHERE EmployeeId = ?1")
	List<EmployeePosition> findByEmployeeId(Long employeeId);
	
	@Query("From EmployeePosition WHERE PositionId = ?1")
	List<EmployeePosition> findByPositionId(Long positionId);
}
