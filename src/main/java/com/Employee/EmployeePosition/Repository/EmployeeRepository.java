package com.Employee.EmployeePosition.Repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.Employee.EmployeePosition.Model.Employee;

public interface EmployeeRepository extends JpaRepository<Employee, Long> {

	@Query("From Employee WHERE BiodataId = ?1")
	List<Employee> findByBiodataId(Long biodataId);
}
