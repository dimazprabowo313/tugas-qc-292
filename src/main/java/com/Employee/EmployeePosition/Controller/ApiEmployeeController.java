package com.Employee.EmployeePosition.Controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.Employee.EmployeePosition.Model.Biodata;
import com.Employee.EmployeePosition.Model.Employee;
import com.Employee.EmployeePosition.Repository.BiodataRepository;
import com.Employee.EmployeePosition.Repository.EmployeeRepository;

@RestController
@CrossOrigin("*")
@RequestMapping("/api")
public class ApiEmployeeController {

	@Autowired
	private EmployeeRepository employeeRepo;
	
	@Autowired
	private BiodataRepository biodataRepo;
	
	@GetMapping("/biodata")
	public ResponseEntity<List<Biodata>> GetAllBiodata(){
		try {
			List<Biodata> biodata = this.biodataRepo.findAll();
			return new ResponseEntity<>(biodata, HttpStatus.OK);
		} catch (Exception exception) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}

	


@PostMapping("/employee")
public ResponseEntity<Object> SaveEmployee(@RequestBody Employee employee) {
	try {
		this.employeeRepo.save(employee);
		return new ResponseEntity<>(employee, HttpStatus.OK);
	} catch (Exception exception) {
		return new ResponseEntity<>("Failed", HttpStatus.BAD_REQUEST);
	}
}

@GetMapping("/employeemapped")
public ResponseEntity<Map<String, Object>> GetAllPage(@RequestParam(defaultValue = "0") int page,
		@RequestParam(defaultValue = "5") int size) {
	try {
		List<Employee> employee = new ArrayList<>();
		Pageable pagingSort = PageRequest.of(page, size);

		Page<Employee> pageTuts;

		pageTuts = employeeRepo.findAll(pagingSort);

		employee = pageTuts.getContent();

		Map<String, Object> response = new HashMap<>();
		response.put("employee", employee);
		response.put("currentPage", pageTuts.getNumber());
		response.put("totalItems", pageTuts.getTotalElements());
		response.put("totalPages", pageTuts.getTotalPages());

		return new ResponseEntity<>(response, HttpStatus.OK);
	}

	catch (Exception e) {
		return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
	}
}

@GetMapping("/employee/{id}")
public ResponseEntity<List<Employee>> GetCategoryById(@PathVariable("id") Long id) {
	try {
		Optional<Employee> category = this.employeeRepo.findById(id);
		if (category.isPresent()) {
			ResponseEntity
			rest = new ResponseEntity<>(category, HttpStatus.OK);
			return rest;
		}
		else {
			return ResponseEntity.notFound().build();
		}

		
	}

	catch (Exception exception) {
		return new ResponseEntity<>( HttpStatus.NO_CONTENT);
	}
}

@PutMapping("/employee/{id}")
public ResponseEntity<Object> UpdateCategory(@RequestBody Employee employee, @PathVariable("id") Long id) 
{
	Optional<Employee> categoryData = this.employeeRepo.findById(id);
	if (categoryData.isPresent()) {
		employee.setId(id);
		this.employeeRepo.save(employee);
		ResponseEntity rest = new ResponseEntity<>("Success", HttpStatus.OK);
		return rest;
	}
	else {
		return ResponseEntity.notFound().build();
	}
}


@DeleteMapping("/employee/{id}")
public ResponseEntity<Object> DeleteCategory(@PathVariable("id") Long id) 
{
	Optional<Employee> categoryData = this.employeeRepo.findById(id);
	if (categoryData.isPresent()) {
		this.employeeRepo.deleteById(id);
		ResponseEntity rest = new ResponseEntity<>("Success", HttpStatus.OK);
		return rest;
	}
	else {
		return ResponseEntity.notFound().build();
	}
}
}
