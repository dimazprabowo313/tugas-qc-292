package com.Employee.EmployeePosition.Controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.Employee.EmployeePosition.Model.Biodata;
import com.Employee.EmployeePosition.Model.Employee;
import com.Employee.EmployeePosition.Model.EmployeePosition;
import com.Employee.EmployeePosition.Model.Position;
import com.Employee.EmployeePosition.Repository.EmployeePositionRepository;
import com.Employee.EmployeePosition.Repository.EmployeeRepository;
import com.Employee.EmployeePosition.Repository.PositionRepository;

@RestController
@CrossOrigin("*")
@RequestMapping("/api")
public class ApiEmployeePositionController {

	@Autowired
	private EmployeePositionRepository employeePosRepo; 
	
	@Autowired
	private EmployeeRepository employeeRepo; 
	
	@Autowired
	private PositionRepository posRepo; 
	
	
	@GetMapping("/employee")
	public ResponseEntity<List<Employee>> GetAllEmployee(){
		try {
			List<Employee> employee = this.employeeRepo.findAll();
			return new ResponseEntity<>(employee, HttpStatus.OK);
		} catch (Exception exception) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}
	
	@GetMapping("/position")
	public ResponseEntity<List<Position>> GetAllPosition(){
		try {
			List<Position> position = this.posRepo.findAll();
			return new ResponseEntity<>(position, HttpStatus.OK);
		} catch (Exception exception) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}
	
	@GetMapping("/employee_position")
	public ResponseEntity<List<EmployeePosition>> GetAllEmployeePosition() {
		try {
			List<EmployeePosition> employee_position = this.employeePosRepo.findAll();
			return new ResponseEntity<>(employee_position, HttpStatus.OK);
		} catch (Exception exception) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}

	@PostMapping("/employee_position")
	public ResponseEntity<Object> SaveEmployee(@RequestBody EmployeePosition employeePosition) {
		try {
			this.employeePosRepo.save(employeePosition);
			return new ResponseEntity<>(employeePosition, HttpStatus.OK);
		} catch (Exception exception) {
			return new ResponseEntity<>("Failed", HttpStatus.BAD_REQUEST);
		}
	}

	@GetMapping("/employeemapped_position")
	public ResponseEntity<Map<String, Object>> GetAllPage(@RequestParam(defaultValue = "0") int page,
			@RequestParam(defaultValue = "5") int size) {
		try {
			List<EmployeePosition> employeePos = new ArrayList<>();
			Pageable pagingSort = PageRequest.of(page, size);

			Page<EmployeePosition> pageTuts;

			pageTuts = employeePosRepo.findAll(pagingSort);

			employeePos = pageTuts.getContent();

			Map<String, Object> response = new HashMap<>();
			response.put("employeePos", employeePos);
			response.put("currentPage", pageTuts.getNumber());
			response.put("totalItems", pageTuts.getTotalElements());
			response.put("totalPages", pageTuts.getTotalPages());

			return new ResponseEntity<>(response, HttpStatus.OK);
		}

		catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@GetMapping("/employee_position/{id}")
	public ResponseEntity<List<EmployeePosition>> GetEmployeePosById(@PathVariable("id") Long id) {
		try {
			Optional<EmployeePosition> employee_position = this.employeePosRepo.findById(id);
			if (employee_position.isPresent()) {
				ResponseEntity rest = new ResponseEntity<>(employee_position, HttpStatus.OK);
				return rest;
			} else {
				return ResponseEntity.notFound().build();
			}

		}

		catch (Exception exception) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}

	@PutMapping("/employee_position/{id}")
	public ResponseEntity<Object> UpdateEmployeePos(@RequestBody EmployeePosition employeePosition,
			@PathVariable("id") Long id) {
		Optional<EmployeePosition> employee_position = this.employeePosRepo.findById(id);
		if (employee_position.isPresent()) {
			employeePosition.setId(id);
			this.employeePosRepo.save(employeePosition);
			ResponseEntity rest = new ResponseEntity<>("Success", HttpStatus.OK);
			return rest;
		} else {
			return ResponseEntity.notFound().build();
		}
	}

	@DeleteMapping("/employee_position/{id}")
	public ResponseEntity<Object> DeleteEmployeePos(@PathVariable("id") Long id) {
		Optional<EmployeePosition> employee_position = this.employeePosRepo.findById(id);
		if (employee_position.isPresent()) {
			this.employeePosRepo.deleteById(id);
			ResponseEntity rest = new ResponseEntity<>("Success", HttpStatus.OK);
			return rest;
		} else {
			return ResponseEntity.notFound().build();
		}
	}
}
