package com.Employee.EmployeePosition.Model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "employee")
public class Employee {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false)
	private Long id;
	
	@Column(name = "biodata_id", nullable = false)
	private Integer BiodataId;
	
	@Column(name = "employee_nip", nullable = false)
	private String EmployeeNip;
	
	@Column(name = "employee_status", nullable = false)
	private String EmployeeStatus;
	
	@Column(name = "employee_salary", nullable = false)
	private Double EmployeeSalary;
	
	@Column(name = "is_delete", nullable = false)
	private Boolean IsDelete = false;

	@OneToOne
	@JoinColumn(name = "biodata_id", insertable = false, updatable = false)
	public Biodata biodata;
	
	//Getter and Setter
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getBiodataId() {
		return BiodataId;
	}

	public void setBiodataId(Integer biodataId) {
		BiodataId = biodataId;
	}

	public String getEmployeeNip() {
		return EmployeeNip;
	}

	public void setEmployeeNip(String employeeNip) {
		EmployeeNip = employeeNip;
	}

	public String getEmployeeStatus() {
		return EmployeeStatus;
	}

	public void setEmployeeStatus(String employeeStatus) {
		EmployeeStatus = employeeStatus;
	}

	public Double getEmployeeSalary() {
		return EmployeeSalary;
	}

	public void setEmployeeSalary(Double employeeSalary) {
		EmployeeSalary = employeeSalary;
	}

	public Boolean getIsDelete() {
		return IsDelete;
	}

	public void setIsDelete(Boolean isDelete) {
		IsDelete = isDelete;
	}
	
	
	
}
