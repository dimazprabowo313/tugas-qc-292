package com.Employee.EmployeePosition.Model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "employee_position")
public class EmployeePosition {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false)
	private Long id;
	
	@OneToOne
	@JoinColumn(name = "employee_id", insertable = false, updatable = false)
	public Employee employee;
	
	@OneToOne
	@JoinColumn(name = "position_id", insertable = false, updatable = false)
	public Position position;
	
	@Column(name = "employee_id", nullable = false)
	private Long EmployeeId;
	
	@Column(name = "position_id", nullable = false)
	private Long PositionId;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	public Position getPosition() {
		return position;
	}

	public void setPosition(Position position) {
		this.position = position;
	}

	public Long getEmployeeId() {
		return EmployeeId;
	}

	public void setEmployeeId(Long employeeId) {
		EmployeeId = employeeId;
	}

	public Long getPositionId() {
		return PositionId;
	}

	public void setPositionId(Long positionId) {
		PositionId = positionId;
	}

	
	
}
