package com.Employee.EmployeePosition.Model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "biodata")
public class Biodata {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false)
	private Long id;
	
	@Column(name = "first_name", nullable = false)
	private String FirstName;
	
	@Column(name = "last_name", nullable = false)
	private String LastName;
	
	@Column(name = "dob", nullable = false)
	private String Dob;
	
	@Column(name = "pob", nullable = false)
	private String Pob;
	
	@Column(name = "address", nullable = false)
	private String Address;
	
	@Column(name = "marital_status", nullable = false)
	private boolean MaritalStatus;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFirstName() {
		return FirstName;
	}

	public void setFirstName(String firstName) {
		FirstName = firstName;
	}

	public String getLastName() {
		return LastName;
	}

	public void setLastName(String lastName) {
		LastName = lastName;
	}

	public String getDob() {
		return Dob;
	}

	public void setDob(String dob) {
		Dob = dob;
	}

	public String getPob() {
		return Pob;
	}

	public void setPob(String pob) {
		Pob = pob;
	}

	public String getAddress() {
		return Address;
	}

	public void setAddress(String address) {
		Address = address;
	}

	public boolean isMaritalStatus() {
		return MaritalStatus;
	}

	public void setMaritalStatus(boolean maritalStatus) {
		MaritalStatus = maritalStatus;
	}
	
	
	
	
	
}
